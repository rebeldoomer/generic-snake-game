# Generic Snake Game

This project is a simple implementation of the classic Snake game using Pygame, very Nokia-style for nostalgia's sake.

## Getting Started

To get started with the Generic Snake Game, clone this repository to your local machine and ensure you have Python and Pygame installed.

### Prerequisites

    Python 3.x
    Pygame

### Installation

1. Clone the repository to your local machine:

`git clone https://gitlab.com/rebeldoomer/generic-snake-game.git`

2. Navigate to the cloned directory:

`cd generic-snake-game`

3. Install Pygame (if you haven't already):

`pip install pygame`

4. Run the game:

`python3 main.py`


## Usage

Use the arrow keys to navigate the snake towards the food. Each time the snake eats the food, it grows in length.

## Contributing

Contributions are what make the open-source community a great place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.


## License

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-nc-sa/4.0/](http://creativecommons.org/licenses/by-nc-sa/4.0/)


## Project Status

This project is in a stable state and is currently maintained. Contributions or suggestions for improvements are welcome.
